package com.example.luxoft.networking;

import com.example.luxoft.di.DaggerApiComponent;
import com.example.luxoft.model.MovieList;

import javax.inject.Inject;

import io.reactivex.Single;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.luxoft.Constants.BASE_URL;

public class MovieAPIService {
    @Inject MoviesApi moviesApi;

    public MovieAPIService() {
        DaggerApiComponent.create().inject(this);
    }

    public Single<MovieList> getMovies() {
        return moviesApi.getMovies();
    }

    public Single<MovieList> search(String searchSequence) {
        return moviesApi.search(searchSequence);
    }
}
