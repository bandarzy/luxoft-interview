package com.example.luxoft.networking;

import com.example.luxoft.model.MovieList;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MoviesApi {
    @GET("movie/now_playing")
    Single<MovieList> getMovies();

    @GET("search/movie")
    Single<MovieList> search(@Query("query") String searchSequence);
}
