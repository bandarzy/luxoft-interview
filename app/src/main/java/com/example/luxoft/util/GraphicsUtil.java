package com.example.luxoft.util;

import android.content.Context;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.luxoft.R;

import static com.example.luxoft.Constants.BASE_IMG_URL;

public class GraphicsUtil {
    public static void loadImage(ImageView imageView,
                                 String url,
                                 CircularProgressDrawable progressDrawable) {
        RequestOptions requestOptions = new RequestOptions()
                .placeholder(progressDrawable)
                .error(R.drawable.ic_baseline_movie_24);

        Glide.with(imageView.getContext())
                .setDefaultRequestOptions(requestOptions)
                .load(url)
                .into(imageView);
    }

    public static CircularProgressDrawable getProgressDrawable(Context context) {
        CircularProgressDrawable cpd = new CircularProgressDrawable(context);
        cpd.setStrokeWidth(10f);
        cpd.setCenterRadius(50f);
        cpd.start();
        return cpd;
    }

    @BindingAdapter("android:imageUrl")
    public static void loadImage(ImageView view, String url) {
        loadImage(view, BASE_IMG_URL + url, getProgressDrawable(view.getContext()));
    }

    @BindingAdapter("android:statusStar")
    public static void loadImage(ImageView view, Boolean liked) {
        view.setImageResource(liked ?
                R.drawable.ic_baseline_star_24 :
                R.drawable.ic_baseline_star_border_24);
    }
}
