package com.example.luxoft.util;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.preference.PreferenceManager;

public class SharedPreferencesHelper {

    private static final String PREF_TIME = "pref_time";
    private SharedPreferences preferences;

    public SharedPreferencesHelper(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void saveUpdateTime(long time) {
        preferences.edit().putLong(PREF_TIME, time).apply();
    }

    public long getUpdateTime() {
        return preferences.getLong(PREF_TIME, 0);
    }
}
