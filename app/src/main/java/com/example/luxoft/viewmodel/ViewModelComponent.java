package com.example.luxoft.viewmodel;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ViewModelModule.class})
public interface ViewModelComponent {
    void inject(DetailViewModel detailViewModel);
    void inject(ListViewModel detailViewModel);
}
