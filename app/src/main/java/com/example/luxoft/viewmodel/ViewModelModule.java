package com.example.luxoft.viewmodel;

import android.content.Context;

import com.example.luxoft.networking.MovieAPIService;
import com.example.luxoft.util.SharedPreferencesHelper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ViewModelModule {
    private final Context context;

    public ViewModelModule(Context context) {
        this.context = context;
    }

    @Provides
    public Context getContext() {
        return this.context;
    }

    @Singleton
    @Provides
    public SharedPreferencesHelper getSharedPrefHelper (Context context) {
        return new SharedPreferencesHelper(context);
    }

    @Provides
    public MovieAPIService getMovieApiService () {
        return new MovieAPIService();
    }
}
