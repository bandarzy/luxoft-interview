package com.example.luxoft.viewmodel;

import android.app.Application;
import android.os.AsyncTask;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.example.luxoft.model.Like;
import com.example.luxoft.model.LikesDao;
import com.example.luxoft.model.Movie;
import com.example.luxoft.networking.MovieAPIService;
import com.example.luxoft.model.MovieDao;
import com.example.luxoft.model.MovieDatabase;
import com.example.luxoft.model.MovieList;
import com.example.luxoft.util.SharedPreferencesHelper;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static com.example.luxoft.Constants.DATA_REFRESH_TIME;

public class ListViewModel extends AndroidViewModel {

    public MutableLiveData<List<Movie>> movies = new MutableLiveData<>();
    public MutableLiveData<Boolean> loadError = new MutableLiveData<>();
    public MutableLiveData<Boolean> isLoading = new MutableLiveData<>();

    private CompositeDisposable mDisposable = new CompositeDisposable();

    // todo: rewrite to not use executor instead of asynctask
    private AsyncTask<List<Movie>, Void, List<Movie>> mInsertTask;
    private AsyncTask<Void, Void, List<Movie>> mRetrieveTask;

    @Inject public SharedPreferencesHelper prefHelper;
    @Inject public MovieAPIService movieAPIService;

    public ListViewModel(@NonNull Application application) {
        super(application);
        DaggerViewModelComponent.builder()
                .viewModelModule(new ViewModelModule(getApplication()))
                .build()
                .inject(this);
    }

    private void fetchFromDatabase() {
        isLoading.setValue(true);
        mRetrieveTask = new RetrieveMoviesTask();
        mRetrieveTask.execute();
    }

    public void fetchFromRemote() {
        isLoading.setValue(true);
        mDisposable.add(
                movieAPIService.getMovies()
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableSingleObserver<MovieList>() {
                            @Override
                            public void onSuccess(MovieList movieList) {
                                mInsertTask = new InsertMoviesTask();
                                mInsertTask.execute(movieList.getResults());
                            }

                            @Override
                            public void onError(Throwable e) {
                                loadError.setValue(true);
                                isLoading.setValue(false);
                                e.printStackTrace();
                            }
                        })
        );
    }

    public void searchInRemote(String searchSequence) {
        isLoading.setValue(true);
        mDisposable.add(
                movieAPIService.search(searchSequence)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableSingleObserver<MovieList>() {
                            @Override
                            public void onSuccess(MovieList movieList) {
                                List<Movie> movies = movieList.getResults();
                                onMoviesRetrieved(movieList.getResults());
                            }

                            @Override
                            public void onError(Throwable e) {
                                loadError.setValue(true);
                                isLoading.setValue(false);
                                e.printStackTrace();
                            }
                        })
        );
    }

    private void onMoviesRetrieved(List<Movie> movieList) {
        movies.setValue(movieList);
        loadError.setValue(false);
        isLoading.setValue(false);
    }

    public void refresh() {
        long updateTime = prefHelper.getUpdateTime();
        long currentTime = System.nanoTime();
        if (updateTime != 0 && currentTime - updateTime < DATA_REFRESH_TIME) {
            fetchFromDatabase();
        } else {
            fetchFromRemote();
        }
    }

    public void forceRefreshFromEndpoint() {
        fetchFromRemote();
    }

    public void search(String searchSequence) {
        searchInRemote(searchSequence);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        mDisposable.clear();

        if (mInsertTask != null) {
            mInsertTask.cancel(true);
            mInsertTask = null;
        }

        if (mRetrieveTask != null) {
            mRetrieveTask.cancel(true);
            mRetrieveTask = null;
        }
    }


    private class InsertMoviesTask extends AsyncTask<List<Movie>, Void, List<Movie>> {

        @Override
        protected List<Movie> doInBackground(List<Movie>... lists) {
            List<Movie> movieList = lists[0];
            MovieDao movieDao = MovieDatabase.getInstance(getApplication()).movieDao();
            movieDao.deleteAllMovies();

            LikesDao likesDao = MovieDatabase.getInstance(getApplication()).likesDao();
            List<Like> likes = likesDao.getAllLikes();

            List<Movie> newList = new ArrayList<>(movieList);
            for (int i=0; i < newList.size(); i++) {
                newList.get(i).setLike(checkIfLiked(newList.get(i).getId(), likes));
            }

            List<Long> result = movieDao.insertAll(newList.toArray(new Movie[0]));

            for (int i=0; i < movieList.size(); i++) {
                movieList.get(i).uuid = result.get(i);
            }
            return movieList;
        }

        @Override
        protected void onPostExecute(List<Movie> movies) {
            onMoviesRetrieved(movies);
            prefHelper.saveUpdateTime(System.nanoTime());
        }

        private boolean checkIfLiked(int movieId, List<Like> likedList) {
            for (Like like : likedList) {
                if (like.getMovieId() == movieId) {
                    return true;
                }
            }
            return false;
        }
    }

    private class RetrieveMoviesTask extends AsyncTask<Void, Void, List<Movie>> {

        @Override
        protected List<Movie> doInBackground(Void... voids) {
            return MovieDatabase.getInstance(getApplication()).movieDao().getAllMovies();
        }

        @Override
        protected void onPostExecute(List<Movie> movies) {
            onMoviesRetrieved(movies);
        }
    }

}
