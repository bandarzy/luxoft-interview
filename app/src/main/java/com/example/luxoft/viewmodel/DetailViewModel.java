package com.example.luxoft.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.luxoft.model.Movie;

public class DetailViewModel extends ViewModel {
    public MutableLiveData<Movie> movieLiveData = new MutableLiveData<>();
}
