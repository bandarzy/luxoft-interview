package com.example.luxoft.model;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface LikesDao {
    @Query("SELECT * FROM `Like`")
    List<Like> getAllLikes();

    @Query("SELECT * FROM `Like` WHERE mMovieId = :movieId")
    Like getLike(Integer movieId);

    @Insert()
    List<Long> insertAll(Like... likes);

    @Query("DELETE FROM `Like`")
    void deleteAllLikes();

    @Query("DELETE FROM `Like` WHERE mMovieId = :movieId")
    void remove(Integer movieId);
}
