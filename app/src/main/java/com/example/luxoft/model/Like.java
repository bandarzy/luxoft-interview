package com.example.luxoft.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;

@Entity
public class Like {
    @PrimaryKey(autoGenerate = true)
    public Long id;

    @Expose
    private Integer mMovieId;

    public Like(Integer movieId) {
        this.mMovieId = movieId;
    }

    public Integer getMovieId() {
        return mMovieId;
    }
}
