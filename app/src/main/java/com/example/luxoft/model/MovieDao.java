package com.example.luxoft.model;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface MovieDao {
    @Insert
    List<Long> insertAll(Movie... movies);

    @Update
    void updateMovie(Movie... movies);

    @Query("SELECT * FROM movie")
    List<Movie> getAllMovies();

    @Query("SELECT * FROM movie WHERE movie_id = :movieId")
    Movie getMovie(int movieId);

    @Query("DELETE FROM movie")
    void deleteAllMovies();
}
