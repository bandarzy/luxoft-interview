package com.example.luxoft.view;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.luxoft.R;
import com.example.luxoft.databinding.FragmentFilmDetailBinding;
import com.example.luxoft.model.Like;
import com.example.luxoft.model.Movie;
import com.example.luxoft.viewmodel.DetailViewModel;

public class FilmDetailFragment extends Fragment {

    private Movie mMovie;
    private DetailViewModel mDetailViewModel;
    private FragmentFilmDetailBinding mViewBinding;

    public FilmDetailFragment() {}

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mViewBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_film_detail, container, false);
        return mViewBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            mMovie = FilmDetailFragmentArgs.fromBundle(getArguments()).getMovie();
        }
        ((MainActivity)getActivity()).setActionBarTitle(mMovie == null ? "" : mMovie.getTitle());
        mDetailViewModel = ViewModelProviders.of(this).get(DetailViewModel.class);
        mDetailViewModel.movieLiveData.setValue(mMovie);
        observeViewModel();
        setLikeButtonListener();
    }

    private void setLikeButtonListener() {
        mViewBinding.fragmentDetailFilmLike.setOnClickListener(v -> {
            mMovie.setLike(!mMovie.isLike());
            mDetailViewModel.movieLiveData.setValue(mMovie);
            new SaveLikeTask(v.getContext()).execute(new Like(mMovie.getId()));
        });
    }

    private void observeViewModel() {
        mDetailViewModel.movieLiveData.observe(getViewLifecycleOwner(), movie -> {
            if (movie != null) {
                mViewBinding.setMovie(movie);
            }
        });
    }
}