package com.example.luxoft.view;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.luxoft.R;
import com.example.luxoft.databinding.FragmentFilmListBinding;
import com.example.luxoft.viewmodel.ListViewModel;

import java.util.ArrayList;

public class FilmListFragment extends Fragment {

    private ListViewModel mViewModel;
    private MovieListAdapter mListAdapter;
    private FragmentFilmListBinding mViewBinding;

    public FilmListFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mViewBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_film_list, container, false);
        return mViewBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((MainActivity)getActivity()).setActionBarTitle(getString(R.string.app_name));
        mListAdapter = new MovieListAdapter(new ArrayList<>(), view.getContext());

        mViewModel = ViewModelProviders.of(this).get(ListViewModel.class);
        mViewModel.refresh();

        mViewBinding.filmListRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mViewBinding.filmListRecyclerView.setAdapter(mListAdapter);

        mViewBinding.filmListRefreshLayout.setOnRefreshListener(() -> {
            mViewBinding.filmListRecyclerView.setVisibility(View.GONE);
            mViewBinding.filmListLoadingProgress.setVisibility(View.GONE);
            mViewBinding.filmListErrorTv.setVisibility(View.GONE);

            mViewModel.forceRefreshFromEndpoint();
            mViewBinding.filmListRefreshLayout.setRefreshing(false);
        });

        mViewBinding.filmListSearchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mViewBinding.filmListRecyclerView.setVisibility(View.GONE);
                mViewBinding.filmListLoadingProgress.setVisibility(View.VISIBLE);
                mViewBinding.filmListErrorTv.setVisibility(View.GONE);
                mViewModel.search(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {}
        });

        observeViewModel();
    }

    private void observeViewModel() {
        mViewModel.movies.observe(getViewLifecycleOwner(), movies -> {
            if (movies != null) {
                mViewBinding.filmListRecyclerView.setVisibility(View.VISIBLE);
                mListAdapter.updateMoviesList(movies);
            }
        });

        mViewModel.loadError.observe(getViewLifecycleOwner(), isError -> {
            if (isError != null) {
                mViewBinding.filmListErrorTv.setVisibility(isError ? View.VISIBLE : View.GONE);
            }
        });

        mViewModel.isLoading.observe(getViewLifecycleOwner(), isLoading -> {
            if (isLoading != null) {
                mViewBinding.filmListLoadingProgress.setVisibility(isLoading ? View.VISIBLE : View.GONE);
                mViewBinding.filmListErrorTv.setVisibility(View.GONE);
                mViewBinding.filmListRecyclerView.setVisibility(isLoading ? View.GONE : View.VISIBLE);
            }
        });
    }
}