package com.example.luxoft.view;

import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.luxoft.R;
import com.example.luxoft.databinding.ItemFilmBinding;
import com.example.luxoft.model.Like;
import com.example.luxoft.model.Movie;

import java.util.List;

public class MovieListAdapter extends RecyclerView.Adapter<MovieListAdapter.MovieViewHolder> {

    private List<Movie> mMoviesList;
    private Context mContext;

    public MovieListAdapter(List<Movie> moviesList, Context context) {
        mContext = context;
        mMoviesList = moviesList;

    }

    public void updateMoviesList(List<Movie> moviesList) {
        mMoviesList.clear();
        mMoviesList.addAll(moviesList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemFilmBinding view = DataBindingUtil.inflate(inflater, R.layout.item_film, parent, false);
        return new MovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder holder, int position) {
        holder.itemView.setMovie(mMoviesList.get(position));

        holder.itemView.itemFilmLayout.setOnClickListener(view -> {
            FilmListFragmentDirections.ActionFilmListFragmentToFilmDetailFragment action =
                    FilmListFragmentDirections.actionFilmListFragmentToFilmDetailFragment(mMoviesList.get(position));
            Navigation.findNavController(holder.itemView.itemFilmLayout).navigate(action);
        });

        holder.itemView.itemFilmLike.setOnClickListener(view -> {
            Like like = new Like(mMoviesList.get(position).getId());
            AsyncTask<Like, Void, Void> saveLikeTask = new SaveLikeTask(mContext);
            saveLikeTask.execute(like);

            mMoviesList.get(position).setLike(!mMoviesList.get(position).isLike());
            holder.itemView.itemFilmLike.setImageResource(mMoviesList.get(position).isLike() ?
                    R.drawable.ic_baseline_star_24 :
                    R.drawable.ic_baseline_star_border_24);

        });
    }

    @Override
    public int getItemCount() {
        return mMoviesList.size();
    }

    static class MovieViewHolder extends RecyclerView.ViewHolder {
        ItemFilmBinding itemView;

        public MovieViewHolder(@NonNull ItemFilmBinding binding) {
            super(binding.getRoot());
            this.itemView = binding;
        }
    }
}
