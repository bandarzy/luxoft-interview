package com.example.luxoft.view;

import android.content.Context;
import android.os.AsyncTask;

import com.example.luxoft.model.Like;
import com.example.luxoft.model.LikesDao;
import com.example.luxoft.model.Movie;
import com.example.luxoft.model.MovieDao;
import com.example.luxoft.model.MovieDatabase;

class SaveLikeTask extends AsyncTask<Like, Void, Void> {

    private Context mContext;

    public SaveLikeTask (Context context) {
        mContext = context;
    }

    @Override
    protected Void doInBackground(Like... likes) {
        Like like = likes[0];
        LikesDao likesDao = MovieDatabase.getInstance(mContext).likesDao();
        Like storedLike = likesDao.getLike(like.getMovieId());
        boolean isLikedBefore = (storedLike != null);

        if (isLikedBefore) {
            likesDao.remove(like.getMovieId());
        } else {
            likesDao.insertAll(like);
        }

        MovieDao movieDao = MovieDatabase.getInstance(mContext).movieDao();
        Movie movie = movieDao.getMovie(like.getMovieId());
        movie.setLike(!isLikedBefore);
        movieDao.updateMovie(movie);
        return null;
    }
}