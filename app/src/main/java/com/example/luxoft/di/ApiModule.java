package com.example.luxoft.di;

import com.example.luxoft.networking.MoviesApi;
import com.example.luxoft.networking.MoviesCallInterceptor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.luxoft.Constants.BASE_URL;

@Module
public class ApiModule {
    @Singleton
    @Provides
    public OkHttpClient getInterceptorClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(new MoviesCallInterceptor())
                .build();
    }

    @Provides
    @Singleton
    public Retrofit getRetrofit(OkHttpClient client) {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build();
    }

    @Provides
    public MoviesApi getMoviesApi(Retrofit retrofit) {
        return retrofit.create(MoviesApi.class);
    }

}
