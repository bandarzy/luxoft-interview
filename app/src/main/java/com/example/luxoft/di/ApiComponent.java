package com.example.luxoft.di;

import com.example.luxoft.networking.MovieAPIService;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApiModule.class})
public interface ApiComponent {
    void inject(MovieAPIService service);
}
