package com.example.luxoft;

public class Constants {
    public static final String BASE_URL = "https://api.themoviedb.org/3/";
    public static final String BASE_IMG_URL = "https://image.tmdb.org/t/p/w500/";
    public static final String API_KEY = "5dd9518a9340836d52228b327613bf94";
    public static final String LANGUAGE = "en-US";

    public static final String MOVIE_DATABASE_NAME = "moviesdatabase";


    public static final long DATA_REFRESH_TIME = 5 * 60 * 1000 * 1000 * 1000L;
}
